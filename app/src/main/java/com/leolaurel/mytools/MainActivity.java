package com.leolaurel.mytools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import static com.leolaurel.mytools.CommandExecution.execCommand;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
    private Context mContext;
    private EditText cmdEdit;
    private TextView tipText,cmdText,webText;
    private CheckBox boxRoot,boxInfo;
    private Button btnCmdExec,btnFastTcp,btnFastUsb;
    private ConstraintLayout mainLayout,websLayout;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplication();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        stringFromJNI();

        initView();
    }

    private void initView() {
        mainLayout = (ConstraintLayout) findViewById(R.id.layout_main);
        websLayout = (ConstraintLayout) findViewById(R.id.layout_webs);

        tipText = (TextView) findViewById(R.id.tip_text);
        cmdText = (TextView) findViewById(R.id.cmd_text);
        cmdEdit = (EditText) findViewById(R.id.cmd_edit);

        webText = (TextView) findViewById(R.id.web_text);
        webView = (WebView)  findViewById(R.id.web_view);
        boxRoot = (CheckBox) findViewById(R.id.box_root);
        boxInfo = (CheckBox) findViewById(R.id.box_info);

        btnCmdExec = (Button) findViewById(R.id.cmd_exec);
        btnFastTcp = (Button) findViewById(R.id.fast_tcp);
        btnFastUsb = (Button) findViewById(R.id.fast_usb);

        btnCmdExec.setOnClickListener(this);
        btnFastTcp.setOnClickListener(this);
        btnFastUsb.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch ( item.getItemId()){
            case R.id.action_main:
                mainLayout.setVisibility(View.VISIBLE);
                websLayout.setVisibility(View.GONE);
                return true;
            case R.id.action_webs:
                mainLayout.setVisibility(View.GONE);
                websLayout.setVisibility(View.VISIBLE);
                showWebView();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showWebView() {
        WebSettings webSetting= webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        //webView.setWebChromeClient(new WebChromeClient());
        webView.setWebChromeClient(new WebChromeClient (){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                Log.d(TAG,"onJsAlert");
                result.confirm();
                return true;
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                Log.d(TAG,"onJsPrompt");
                result.confirm();
                return true;
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                Log.d(TAG,"onShowFileChooser");
                Intent intent = fileChooserParams.createIntent();
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                Log.d(TAG,"onJsConfirm");
                return true;
            }
        });
        webView.loadUrl("file:///android_asset/index.html");
        //webView.loadUrl("http://www.jq22.com/yanshi2247");
    }


    @Override
    protected void onResume() {
        super.onResume();
        tipText.setText(getString(R.string.tip_text,getIpAddress(),getMacAddress()));

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    @Override
    public void onClick(View v) {
        String cmd;
        switch (v.getId()){
            case R.id.cmd_exec:
                cmd =  cmdEdit.getText().toString();
                if(cmd.length()==0) return;
                new CommandTask().execute(cmd);
                break;
            case R.id.fast_tcp:
                cmdEdit.setText(cmd = "adb tcpip 5555");
                new CommandTask().execute(cmd);
                break;
            case R.id.fast_usb:
                cmdEdit.setText(cmd = "adb usb");
                new CommandTask().execute(cmd);
                break;
        }
    }

    public String getMacAddress() {
        return CommandExecution.execCommand("cat /sys/class/net/wlan0/address", false).successMsg;
    }

    /**
     * 获取ip地址
     * @return
     */
    public String getIpAddress() {
        NetworkInfo info = ((ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {//当前使用2G/3G/4G网络
                try {
                    for (Enumeration<NetworkInterface> en = NetworkInterface
                            .getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface network = en.nextElement();
                        for (Enumeration<InetAddress> enumeration = network.getInetAddresses();
                             enumeration.hasMoreElements(); ) {
                            InetAddress inetAddress = enumeration.nextElement();
                            if (!inetAddress.isLoopbackAddress() 
                                    && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {//当前使用无线网络
                WifiManager wifiManager = (WifiManager) getApplicationContext()
                        .getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                return intIP2StringIP(wifiInfo.getIpAddress());//得到IPV4地址
            }
        }
        return "127.0.0.1";
    }

    /**
     * 将得到的int类型的IP转换为String类型
     */
    public String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }



    private class CommandTask extends AsyncTask<String,Integer,CommandExecution.CommandResult> {
        private boolean isRoot,isInfo;

        /**
         * 运行在UI线程中，在调用doInBackground()之前执行
         */
        @Override
        protected void onPreExecute() {
            isRoot = boxRoot.isChecked();
            isInfo = boxInfo.isChecked();
            btnCmdExec.setClickable(false);
            btnFastTcp.setClickable(false);
            btnFastUsb.setClickable(false);
        }
        /**
         * 后台运行的方法，可以运行非UI线程，可以执行耗时的方法
         */
        @Override
        protected CommandExecution.CommandResult doInBackground(String... params) {
            return CommandExecution.execCommand(params, isRoot);
        }

        /**
         * 运行在ui线程中，在doInBackground()执行完毕后执行
         */
        @Override
        protected void onPostExecute(CommandExecution.CommandResult result) {
            btnCmdExec.setClickable(true);
            btnFastTcp.setClickable(true);
            btnFastUsb.setClickable(true);
            if(isInfo && result != null){
                if(result.result == 0){
                    cmdText.setText(result.successMsg);
                }else if(result.result>0){
                    cmdText.setText(result.errorMsg);
                }else{
                    cmdText.setText("未知情况");
                }
            }
        }
    }
}
